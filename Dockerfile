FROM golang AS builder

COPY . .

ENV CGO_ENABLED=0
RUN go get -d -v
RUN go build -o /pi-temp

FROM scratch
COPY --from=builder /pi-temp /pi-temp
ENTRYPOINT ["/pi-temp"]
